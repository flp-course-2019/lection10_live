package json

import io.circe._
import io.circe.generic.auto._
import io.circe.parser._
import io.circe.syntax._

import sttp.client._


object Step07CirceSttp extends App {

  implicit val backend = HttpURLConnectionBackend()

  val request = basicRequest
    .get(uri"https://httpbin.org/get")

  case class Bar(accept: String, host: String)
  case class Foo(origin: String, headers: Bar)

  implicit val decoderBar = Decoder.forProduct2("Accept", "Host")(Bar)

  println(request.send().body.map(decode[Foo]))

}
