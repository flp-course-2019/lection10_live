package json
import io.circe.generic.auto._
import io.circe.parser._
import io.circe.syntax._

object Step07Circe extends App {

  case class Person(name: String, age: Int, children: List[Person])

  val p = Person("Volodya", 42, Nil)

  val json = p.asJson

  println(json)

  /*
  val stringJson =
    """
    {
     "name" : "Volodya",
     "age"  : 42,
     "childs" : [
       {
         "name" : "Egor",
         "age"  : 8,
         "childs" : []
       }
     ],
    }
    """

  val fromJson = decode[Person](stringJson)

  println(fromJson)
   */
}
