package json

import io.circe._
import io.circe.generic.auto._
import io.circe.parser._
import io.circe.syntax._

object Step07Circe2 extends App {

  val stringJson2 =
    """
    {
     "name" : "Volodya",
     "age"  : 42,
     "sex" : "Male"
    }
    """

  sealed trait Sex
  case object Male   extends Sex
  case object Female extends Sex

  case class Person(name: String, age: Int, sex: Sex)

  implicit val sexDecoder: Decoder[Sex] = Decoder.decodeString.emap {
    _.toLowerCase match {
      case "male"   => Right(Male)
      case "female" => Right(Female)
      case str      => Left(s"Не удалось распарсить $str")
    }
  }

  val fromJson = decode[Person](stringJson2)

  println(fromJson)

}
