package IO

import cats.effect.ExitCase.Canceled
import cats.effect.{ExitCode, IO, IOApp}

object Step03Cancellable extends IOApp {

  val io1 = IO {
    println("Пошли в один сервис")
    Thread.sleep(2000)
    println("Закончили работу")
    "Done"
  }

  val io2 = IO {
    println("Пошли в другой сервис")
    Thread.sleep(2000)
    println("Закончили другую работу")
    "Done 2"
  }

  val resultIO = for {
    i <- io1
    j <- io2
  } yield i + System.lineSeparator() + j

  override def run(args: List[String]): IO[ExitCode] =
    resultIO.guaranteeCase {
      case Canceled => IO(println("Interrupted"))
      case _        => IO(println("Normal exit"))
    }.as(ExitCode.Success)
}
