package IO

import java.io.File

import cats.effect.{ExitCode, IO, IOApp}

import scala.io.{BufferedSource, Source}

object Step03Cancellable2 extends IOApp {

  val fileIO = IO {
    Source.fromFile(new File("C:\\test\\test.txt"))
  }

  val readFile: BufferedSource => IO[List[String]] = bs => IO {
    println("Начинаем работу c файлом")
    Thread.sleep(2000)
    bs.getLines().toList
  }


  val resIO = for {
    lines <- fileIO.bracket(readFile)(file => IO {
      file.close()
      println("Закрыли файл")
    })
    _     <- IO {
      lines.foreach(println)
    }
  } yield ()

  /*
  def openFile(path: String): Resource[IO, BufferedSource] =
    Resource.make(IO(Source.fromFile(new File(path))))(file => IO {file.close()})

  val resIO2 = for {
    lines <- openFile("C:\\test\\test.txt").use(readFile)
    _     <- IO (lines.foreach(println))
  } yield ()
   */


  override def run(args: List[String]): IO[ExitCode] =
    resIO.as(ExitCode.Success)
}
