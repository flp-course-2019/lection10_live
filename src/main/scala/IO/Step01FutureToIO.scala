package IO


import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

object Step01FutureToIO extends App {

  val f1 = Future {
    println("Пошли в один сервис")
    Thread.sleep(1000)
    42
  }

  val f2 = Future {
    println("Пошли в другой сервис")
    Thread.sleep(1000)
    84
  }

  val result = for {
    i <- f1
    j <- f2
  } yield i + j

  println(Await.result(result, Duration.Inf))

}

