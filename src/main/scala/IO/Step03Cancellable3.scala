package IO

import cats.effect.{ExitCode, IO, IOApp}

object Step03Cancellable3 extends IOApp {

  val work = for {
    _ <- IO(println("Начал работу"))
    _ <- IO(Thread.sleep(2000))
    _ <- IO(println("Продолжаю работу"))
    _ <- IO(Thread.sleep(2000))
    _ <- IO(println("Закончил работу"))
  } yield ()

  val test = for {
    i <- work.start
    _ <- IO.race(i.join, IO(Thread.sleep(1000)))
  } yield ()

  override def run(args: List[String]): IO[ExitCode] =
    test.as(ExitCode.Success)
}
