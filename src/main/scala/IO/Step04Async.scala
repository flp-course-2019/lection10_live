package IO

import cats.effect.IO

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future, Promise}

object Step04Async extends App {

  val ec = scala.concurrent.ExecutionContext.global
  val cs = IO.contextShift(ec)

  val a = Promise[Int]()

  ec.execute( () =>
    try {
      scala.io.StdIn.readLine().toLowerCase() match {
        case "stop" => a.failure(new Exception("stopped"))
        case _      => a.success(0)
      }
    } catch {
      case thr: Throwable => a.failure(thr)
    }
  )

  val result = Future.firstCompletedOf(List(a.future, Future {Thread.sleep(5000); 42}(ec)))(ec)

  println(Await.result(result, Duration.Inf))

  /*
  val ioAsync = for {
    i <- IO.async { (cb: Either[Throwable, String] => Unit) =>
      ec.execute (() =>
        try {
          scala.io.StdIn.readLine().toLowerCase() match {
            case "stop" => cb(Left(new Exception("stopped")))
            case x      => cb(Right(x))
          }
        } catch {
          case thr: Throwable => cb(Left(thr))
        }
      )
    }.start(cs)
    j <- IO {println("I do something"); Thread.sleep(2000); println("Done")}.start(cs)
    _ <- IO.race(i.join, j.join)(cs)
  } yield ()

  ioAsync.unsafeRunSync()
   */
}
