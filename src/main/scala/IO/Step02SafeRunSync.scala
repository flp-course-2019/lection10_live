package IO

import cats.effect.{ExitCode, IO, IOApp}

object Step02SafeRunSync extends IOApp {

  val io1 = IO {
    println("Пошли в один сервис")
    Thread.sleep(1000)
    42
  }

  val io2 = IO {
    println("Пошли в другой сервис ")
    Thread.sleep(1000)
    42
  }

  val resultIO = for {
    start1 <- io1.start
    start2 <- io2.start
    res1   <- start1.join
    res2   <- start2.join
  } yield res1 + res2

  override def run(args: List[String]): IO[ExitCode] =
    resultIO.as(ExitCode.Success)
}
