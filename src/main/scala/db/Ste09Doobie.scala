package db

import java.sql.DriverManager

import cats.effect.IO
import doobie.util.transactor.Transactor
import doobie._
import doobie.implicits._

object Ste09Doobie extends App {

  implicit val cs = IO.contextShift(scala.concurrent.ExecutionContext.global)

  val xa = Transactor.fromDriverManager[IO](
    "org.sqlite.JDBC", "jdbc:sqlite:memory.db"
  )
  val a = sql"create table if not exists testTable(foo TEXT, bar INTEGER, lol REAL)"

  val b = sql"insert into testTable(foo) values ('check')"

  val c = sql"select * from testTable"

  val zzz = xa.yolo
  import zzz._

  sealed trait Foo
  case object Bar   extends Foo
  case object Lol   extends Foo
  case object Other extends Foo

  implicit val readerK: Get[Foo] = Get[String].map {
    case "check" => Bar
    case "lol"   => Lol
    case   _     => Other
  }

  case class A(a: String, b: Option[Int], c: Option[Float])//, d: Option[Double])

  a.update.run.transact(xa).unsafeRunSync()
  b.update.run.transact(xa).unsafeRunSync()
//  println(c.query[A].check.unsafeRunSync())//.to[List]
  println(c.query[A].to[List].transact(xa).unsafeRunSync())

}
