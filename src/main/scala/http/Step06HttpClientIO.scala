package http

import cats.effect.{ExitCode, IO, IOApp}
import org.asynchttpclient.DefaultAsyncHttpClient
import sttp.client._
import sttp.client.asynchttpclient.cats.AsyncHttpClientCatsBackend

object Step06HttpClientIO extends IOApp {

  val a = Map("a" -> "b", "c" -> "d")

  val c = "Lol kek cheburek"

  val b = uri"https://httpbin.org/get?$a" == uri"https://httpbin.org/get?a=b&c=d&z=$c"

  val describeRequest =
    basicRequest.body("test")
                .post(uri"https://httpbin.org/post?hello=world")

  val requestIO = AsyncHttpClientCatsBackend.resource[IO]().use { implicit backend =>
    for {
      response <- describeRequest.send()
      _        <- IO(response.body.foreach(println))
    } yield ()
  }

  def foo[A](implicit ev: A =:= Int) = ???

  override def run(args: List[String]): IO[ExitCode] =
    requestIO.as(ExitCode.Success)
}
