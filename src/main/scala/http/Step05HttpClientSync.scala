package http
import sttp.client._

object Step05HttpClientSync extends App {

  implicit val backend = HttpURLConnectionBackend()

  val requestGet = basicRequest.get(uri"https://httpbin.org/get?lol=arbidol&kek=cheburek")

  val requestPost =
    basicRequest.post(uri"https://httpbin.org/post?lol=arbidol&kek=cheburek")
                .body("test")

  println(requestPost.send())

}
